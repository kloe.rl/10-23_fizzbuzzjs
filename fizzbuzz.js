// Functions permettant de récupérer la valeur de nombre et d'ajouter cette valeur dans le tableau nombres
function multiple3() {
    nombre = "Fizz"
    nombres.push(nombre)
}

function multiple5() {
    nombre = "Buzz"
    nombres.push(nombre)
}

function multiple3_5() {
    nombre = "FizzBuzz"
    nombres.push(nombre)
}

// Déclaration de la variable nombre
var nombre

// Création d'un tableau qui contiendra toutes les valeurs de la boucle FOR
let nombres = []

// Boucle FOR qui va répéter l'action console.log tant que i est inférieur à 101 tout en ajoutant 1 à chaque passage
for (i = 1; i < 101; i++) {
    // Condition qui vérifie si le nombre est un multiple de 3 ET de 5
    if (i % 3 === 0 && i % 5 === 0) {
        multiple3_5()
    // Condition qui vérifie si le nombre est un multiple de 5
    } else if(i % 5 === 0) {
        multiple5()
    // Condition qui vérifie si le nombre est un multiple de 3
    } else if (i % 3 === 0) {
        multiple3()
    // Condition qui s'applique lorsque le nombre ne correspond à aucune autre condiiton
    } else {
        nombre = i
        nombres.push(nombre)
    }
}

// Récupération des éléments HTML (<button> et <p>)
let btnAction = document.getElementById("btnAction")
let text = document.getElementById("text")

// Utilisation de la méthode addEventListener pour afficher le tableau au clic
btnAction.addEventListener("click", () => {
    // A chaque itération, l'élément du tableau est ajouté à la balise <p> puis renvoie à la ligne
    for(var i = 0; i < nombres.length; i++) {
        document.getElementById("text").innerHTML += nombres[i] + "<br>"
    }
})